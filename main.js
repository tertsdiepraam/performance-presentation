import "./style.scss";
import "reveal.js/css/reveal.scss";
import "./code.css";

import Reveal from "reveal.js";
import Markdown from "reveal.js/plugin/markdown/markdown.esm.js";
import Highlight from "reveal.js/plugin/highlight/highlight.esm.js";
import RevealMath from "reveal.js/plugin/math/math.esm.js";

let deck = new Reveal({
  plugins: [Markdown, Highlight, RevealMath],
});
deck.initialize();

const controllers = {};
const lastButtons = {};

function connecthandler(e) {
  addgamepad(e.gamepad);
}

function addgamepad(gamepad) {
  lastButtons[gamepad.index] = { 7: false, 8: false, 9: false, 10: false };
  controllers[gamepad.index] = gamepad;
}

function disconnecthandler(e) {
  removegamepad(e.gamepad);
}

function removegamepad(gamepad) {
  delete lastButtons[gamepad.index];
  delete controllers[gamepad.index];
}

window.addEventListener("gamepadconnected", connecthandler);
window.addEventListener("gamepaddisconnected", disconnecthandler);

function updateStatus() {
  for (const controller of Object.values(controllers)) {
    const buttons = controller.buttons;
    const pressed = (n) => {
      return buttons[n].pressed && !lastButtons[controller.index][n];
    };

    if (pressed(7)) {
      deck.up();
    } else if (pressed(8)) {
      deck.down();
    } else if (pressed(9)) {
      deck.left();
    } else if (pressed(10)) {
      deck.right();
    }
  }
  for (const controller of Object.values(controllers)) {
    const buttons = controller.buttons;
    lastButtons[controller.index][7] = buttons[7].pressed;
    lastButtons[controller.index][8] = buttons[8].pressed;
    lastButtons[controller.index][9] = buttons[9].pressed;
    lastButtons[controller.index][10] = buttons[10].pressed;
  }
  requestAnimationFrame(updateStatus);
}

updateStatus();

const colors = [
  ["#ea76cb", "#7287fd"],
  ["#8839ef", "#d20f39"],
  ["#e64553", "#fe640b"],
  ["#df8e1d", "#179299"],
  ["#209fb5", "#40a02b"],
  ["#dc8a78", "#dd7878"],
  ["#04a5e5", "#1e66f5"],
];

let background2 = false;

function setColors(h, v) {
  const [color1, color2] = colors[h % colors.length];
  if (!background2) {
    document.documentElement.style.setProperty("--bg-color2-1", color1);
    document.documentElement.style.setProperty("--bg-color2-2", color2);
    bg2.style.opacity = 1;
  } else {
    document.documentElement.style.setProperty("--bg-color1-1", color1);
    document.documentElement.style.setProperty("--bg-color1-2", color2);
    bg2.style.opacity = 0;
  }
  background2 = !background2;

  if (v == 0) {
    document.getElementById("bg3").style.opacity = 0;
    document.documentElement.style.setProperty("--r-main-color", "#eeeeee");
    document.documentElement.style.setProperty("--r-heading-color", "#eeeeee");
  } else {
    document.getElementById("bg3").style.opacity = 1;
    document.documentElement.style.setProperty("--r-main-color", "#4c4f69");
    document.documentElement.style.setProperty("--r-heading-color", "#4c4f69");
  }
}

const bg2 = document.getElementById("bg2");

deck.on("slidechanged", () => {
  const state = deck.getState();
  const h = state.indexh;
  const v = state.indexv;
  const url = new URL(window.location.href);
  const params = url.searchParams;
  params.set("h", h);
  params.set("v", v);
  history.pushState(`Slide ${h}, ${v}`, undefined, url);
  setColors(h, v);
});

deck.on("ready", () => {
  const url = new URL(window.location.href);
  const params = url.searchParams;
  const h = params.get("h") ?? 0;
  const v = params.get("v") ?? 0;
  deck.setState({ indexh: h, indexv: v });
  setColors(h, v);
});
