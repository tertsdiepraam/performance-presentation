## Big $O(h\cdot my\cdot god!)$

or how bad performance is holding us back

=V=

You have to interrupt me!

I don't have to get to the end

=V=

About me

## I hate waiting

=V=

I joined a new project...

<div class="fragment">
...and performance was terrible...
</div>

<div class="fragment">

...and I mean many **hours** of runtime

</div>

=V=

### We can do better!

<div class="fragment">

And I'm talking orders of magnitude faster!

</div>

=V=

### And we did!

| Component   | Unoptimized | Optimized |
| ----------- | ----------: | --------: |
| Stats       |         >1h |        4m |
| Sampling    |         80m |        7m |
| XML parsing |  Many hours |      100s |
| Grouping    |          1h |       30s |
| N-point     |  Many hours |       40s |

=V=

Bad performance makes...

<ul>
<li class="fragment">...development slower</li>
<li class="fragment">...upscaling difficult</li>
<li class="fragment">...onboarding annoying</li>
<li class="fragment">...demoing impossible</li>
<li class="fragment">...me angry</li>
</ul>

=V=

Convinced that this is a problem?

=V=

#### Takeaways

- I need you to care!
- If that's all you take from this presentation I'm happy
- Please care about performance!

===

<div class="title">

Part 0

## Profiling

</div>

=V=

Operations that are slow

- Algorithms with large time complexity
- IO
- Copying objects
- Parsing text data
- Allocating and freeing memory
- Interprocess communication
- Python

=V=

To figure out what's going on:

## [Scalene](https://pypi.org/project/scalene/)

=V=

![](scalene.png)

=V=

Amazing talks about measuring performance by Emery Berger:

- ["Performance Matters"](https://youtu.be/r-TLSBdHe1A)
- ["Python Performance Matters"](https://youtu.be/vVUnCXKuNOg)

=V=

#### Takeaways

- Measure performance rigorously
- Use Scalene
- WATCH THE TALKS ON THE PREVIOUS SLIDE THEY ARE SO GOOD

===

<div class="title">

Part 1

## Time Complexity

</div>

=V=

#### A hand-wavy definition

If `foo` has time complexity $O(f(n))$ then the runtime grows "like" $f$ when
the input gets larger

=V=

#### Examples

| Operation                    | Complexity   |
| ---------------------------- | ------------ |
| addition                     | $O(1)$       |
| loop                         | $O(n)$       |
| linear search                | $O(n)$       |
| binary search                | $O(\log n)$  |
| sort                         | $O(n\log n)$ |
| better throw your laptop out | $O(n^2)$     |

=V=

### Quiz: What is the complexity?

```python
node_ids: List[str] = [...]
nodes: List[Node] = [...]

edges: List[Edge] = [...]
for edge in edges:
    idx = node_ids.index(edge.node_id)
    edge.node = nodes[idx]
```

=V=

### Data structures

```python
node_ids: List[str] = [...]
nodes: List[Node] = [...]

edges: List[Edge] = [...]
for edge in edges:                     # O(e)
    idx = node_ids.index(edge.node_id) # O(n)
    edge.node = nodes[idx]             # O(1)
```

$O(e\cdot (n + 1)) = O(e\cdot n) \approx O(n^2)$

=V=

### Data structures

```python
nodes: Dict[str, Node] = {...}

edges: List[Edge] = [...]
for edge in edges:                   # O(e)
    edge.node = nodes[edge.node_id]  # O(1)
```

Total: $O(e)$

=V=

### Hidden loops

```python
[1, 2, 3, 4, 5].index(element)

df["foo"] == 5

list(map(lambda x: 5*x, range(100)))

df["number"].astype("int")

5 in [1, 2, 3, 4, 5]
```

=V=

### Quiz: What's the complexity?

```python
concatenated = ""
for w in words:
    concatenated = w + concatenated
```

<div class="fragment">

```python
concatenated = ""
for w in words:
    concatenated = concatenated + w
```

</div>

=V=

### Answer

<div style="display: flex; flex-direction: row;">
<img src="string_complexity.png" alt="" style="width: 50%">
<img src="string_complexity2.png" alt="" style="width: 50%">
</div>

Left: prepend | Right: append

It's another hidden loop!

=V=

#### Takeaways

- (Hidden) loops are evil
- Think about the complexity of operations
- Think about data structures

===

<div class="title">

Part 2

## Pandas & Numpy

</div>

=V=

Python is slow

=V=

Do as much as possible in Numpy and Pandas

which is harder than it sounds!

=V=

### A bad example (1/2)

```python
# df has int columns x1, y1, x2, y2
# and 1_000_000 rows

# Version A
df["x3"] = df["x1"] + df["x2"]
df["y3"] = df["y1"] + df["y2"]

# Version B
df["a"] = df[["x1", "y1"]] \
    .apply(lambda x: Point(*x), axis=1)
df["b"] = df[["x2", "y2"]] \
    .apply(lambda x: Point(*x), axis=1)

df["c"] = df["a"] + df["b"]
```

=V=

### A bad example (2/2)

| Operation           |    Time |
| ------------------- | ------: |
| Constructing points | 14508ms |
| Numpy addition      |     7ms |
| Point addition      |   710ms |

Numpy is 100x faster

(excluding construction of Points)

=V=

### Apply vs ufuncs

```python
df["bar"] = df["foo"].apply(lambda x: x / 60)
df["baz"] = df["foo"] / 60
```

- apply: 2467ms
- ufunc: 32ms

ufunc is 80x faster

=V=

### Pandas copies a lot

```python
import numpy as np
import pandas as pd

df = pd.DataFrame({
    "a": np.random.randint(0, 100, size=100)
})
v = df.values
df.drop_duplicates(inplace=True)

print(len(df.shape))  # => 65
print(len(v.shape))   # => 100
```

Even when using `inplace=True`!

Always make your `df` as small as possible

=V=

### IO: Don't use CSV

<div class="fragment">

- It's slow
- It's big
- It's untyped
- It's unreadable anyway
- It's inconsistent

</div>

=V=

Example with a big CSV

|         | reading | writing |
| ------- | ------: | ------: |
| csv     |  37 sec |  71 sec |
| feather |   5 sec |   4 sec |

Use binary formats instead (e.g. feather)

=V=

#### Takeaways

- Measure Pandas performance
- Don't use Python data types and functions
- Keep dataframes as small as possible
- Don't use CSV

===

<div class="title">

Part 3

## Less is more

</div>

=V=

Pipelines have tasks with complex dependencies

<p class="fragment">
What if we skip what we don't need?
</p>

=V=

### `pydoit`

- Define tasks with inputs and outputs
- `pydoit` figures out the rest

=V=

In a file called `dodo.py`:

```python
def task_name():
    return {
        "actions": [...]  # python functions or cli commands
        "file_dep": [...] # inputs
        "targets": [...]  # outputs
    }
```

=V=

`dodo.py`

```python
def task_convert():
    return {
        "actions": [convert],
        "file_dep": ["a.csv"],
        "targets": ["a.feather"],
    }

def task_clean_data():
    return {
        "actions": [clean],
        "file_dep": ["a.feather"],
        "targets": ["a_clean.feather"],
    }

def task_process():
    return {
        "actions": [process],
        "file_dep": ["a_clean.feather"],
        "targets": ["result.feather"],
    }
```

=V=

Command line:

```shell
❯ doit list
clean_data
convert
process

❯ doit convert
.  convert

❯ doit convert
-- convert

❯ doit process
-- convert
.  clean_data
.  process
```

=V=

```python
def task_convert_to_feather():
    for file in ["a.csv", "b.csv", "c.csv"]:
        yield {
            "name": file,
            "actions": [(convert_files, file)],
            "file_dep": [file],
            "targets": [Path(file).with_suffix(".feather")],
        }

def task_combine_a_and_b():
    return {
        "actions": [combine],
        "file_dep": ["a.feather", "b.feather"],
        "targets": ["a_and_b.feather"],
    }
```

=V=

```shell
❯ doit list --all
combine_a_and_b            
convert_to_feather         
convert_to_feather:a.csv   
convert_to_feather:b.csv   
convert_to_feather:c.csv
❯ doit combine_a_and_b
.  convert_to_feather:a.csv
.  convert_to_feather:b.csv
.  combine_a_and_b
```

=V=

#### Takeaways

- Use `pydoit` for pipelines
- It has many more features than I demoed

===

<div class="title">

Part 4

## Concurrency & Parallelism

</div>

=V=

We are getting **more** cores, not **faster** cores

<div class="fragment">

We need parallelism to take advantage of this

</div>

=V=

What's the difference?

- **Parallel**: execute at the same time
- **Concurrent**: execute in any order (including parallel)

=V=

#### Concurrency in Python

<ul>
<li>
    <code>threading</code>
    <ul class="fragment">
        <li>Parallel, sort of, but mostly concurrent</li>
        <li>GIL stops parallel execution</li>
        <li>Good for IO-heavy tasks</li>
    </ul>
</li>
<li>
    <code>multiprocessing</code>
    <ul class="fragment">
        <li>Actually parallel</li>
        <li>Interprocess communication is slow and requires pickling</li>
    </ul>
</li>
</ul>

=V=

When is parallel execution a good idea?

1. Tasks that don't depend on each other
2. Split one task over multiple cores

=V=

Example: independent tasks

```python
import time

def a() -> int:
    time.sleep(5)
    return 5

def b() -> int:
    time.sleep(4)
    return 4

print(a() + b())
```

=V=

Example: independent tasks

```python
import time
from multiprocessing import Pool

def a() -> int:
    time.sleep(5)
    return 5

def b() -> int:
    time.sleep(4)
    return 4

with Pool(processes=2) as pool:
    task_a = pool.apply_async(a)
    task_b = pool.apply_async(b)
    print(task_a.get() + task_b.get())
```

=V=

### Parallelism in `pydoit`

Super easy, barely an inconvenience!

```
doit run -n 8 [task]
```

=V=

Example: aggregating multiple data files

```python
import pandas as pd

paths = Path("some/directory").glob("*.csv")

dfs = []
for p in paths:
    df = pd.read_csv(p)
    dfs.append(df)

print(pd.concat(dfs).agg({"names": "count"})
```

=V=

Example: aggregating multiple data files

```python
import pandas as pd
from multiprocessing import Pool

def read_and_aggregate(p: Path):
    return pd.read_csv(p).agg({"names": "count"})

paths = Path("some/directory").glob("*.csv")

with Pool() as pool:
    dfs = pool.map(read_and_aggregate, paths)

pd.concat(dfs).agg({("names", "count"): "sum"})
```

Roughly 8x speedup!

=V=

### What about this?

```python
# Setup
n = 50_000_000
list_1 = list(range(n))
list_2 = list(range(n, 2*n))
```

```python
# Sequential
print(max(max(list_1), max(list_2)))
```

```python
# Parallel
from multiprocessing import Pool
with Pool() as pool:
    res1 = pool.apply_async(max, [list_1])
    res2 = pool.apply_async(max, [list_2])
    print(max(res1.get(), res2.get()))
```

=V=

|            |    |
| ---------- | -- |
| Sequential | 1s |
| Parallel   | 5s |
|            |    |

Why? 🤔

=V=

### Parallelism has overhead

- Spawning process
- Sending data to the process (via pickling)
- Overhead is bigger on macOS than Linux?

=V=

### Parallelism in Native Code

Work around the GIL with native parallelism:

[`dask`](https://www.dask.org/) & [`polars`](https://www.pola.rs/)

=V=

#### Takeaways

- Use parallelism as a _last_ improvement
- Beware of overhead of parallelism
- Use libraries with parallelism when possible

===

<div class="title">

Part 5

## Rewrite it in Rust

</div>

=V=

#### Why Rust?

<small>

- Excellent type system
- Super fast performance
- Fearless concurrency
- Actual parallelism with threads
- Nice community
- No segmentation faults
- Super nice tooling (rustfmt, clippy)
- Awesome package manager (cargo)
- No virtual environments
- Runs on everything
- Ecosystem full of great crates
- No breaking changes in the language
- Dependencies automatically work with new versions
- More confidence in correctness
- Functions and types can actually be private
- Enums easily restrict options and make for better APIs
- Structs are much lighter than classes
- No confusing class inheritance
- No unexpected exceptions
- Low cost of running on server
- Zero-cost abstractions
- No need for dependencies to write fast code
- No hashmaps everywhere
- No interpreter needed
- It teaches best practices by design
- Run it on a webpage via WASM
- Did I mention that it's super fast?

</small>

=V=

I'm joking, but sometimes Python just doesn't cut it

1. Process a file in Rust and output a new file
1. Use [`PyO3`](https://github.com/PyO3/pyo3)

===

### What you have to remember

=V=

- Measure and profile
- Think about complexity
- Use pandas and numpy effectively
- Use pydoit
- Use parallelism when appropriate

===

<small>

and now I'm exhausted

please care about performance

thank you

(below is bonus content)

</small>

=V=

#### Bonus: Tools for faster code

- [Polars](https://www.pola.rs/): new dataframe library
- [Dask](https://www.dask.org/): pandas but parallel and lazy
- [Lxml](https://lxml.de/): Faster XML
- [Numba](https://numba.pydata.org/): JIT compile Python code
- [Pypy](https://www.pypy.org/): JIT interpreter
- [Perflint](https://pypi.org/project/perflint/): finds common performance problems

=V=

#### Bonus: Resources

- [pythonspeed.com](https://pythonspeed.com) has a lot of good advice
- ["Write faster Python! Common performance anti-patterns"](https://youtu.be/YY7yJHo0M5I) by Anthony Shaw
- [Time Complexity](https://wiki.python.org/moin/TimeComplexity) in the Python Wiki
- [Enhancing Performance](https://pandas.pydata.org/docs/user_guide/enhancingperf.html) in Pandas documentation
